import 'dart.io'
num evaluate_posfix(var posfixExpression) {
  var token = posfixExpression;
  var resultList =[];
  var tokenlist = token.split(' ');

  for (var char in tokenlist) {     //วนลูปเพื่อรับค่าแล้วเช็คถ้าเป็นมีเครื่องหมายตัวไหนให้นำมาทำเช่น [2,2,+] = 2+2 แต่ถ้าเป็น [2,+]อย่างงี้จะไม่ได้
    if (double.tryParse(char) != null) {
      resultList.add(double.tryParse(char));
    }else{
      try{
        num sum = 0;
        num sideRight = resultList.removeLast();
        num sideLeft = resultList.removeLast();
        if (char == '+') {
          sum = sideLeft+sideRight;
        }else if(char == '-'){
          sum = sideLeft-sideRight;
        }else if(char == '*'){
          sum = sideLeft*sideRight;
        }else if(char == '/'){
          sum = sideLeft/sideRight;
        }
        resultList.add(sum);
      }catch(e){
        print("According to my calculations, the postfix expression $posfixExpression evaluates to NaN.");  //ถ้าเกิดเหตุการณ์ [2,+] จะprint บรรทัดนี้ออกมา
        return 0;
      }
    }
  }
  return resultList[0];  //ค่าที่print ออกมาจะเป็นตัวแรก เช่น [10,5,17,50] จะprintออกมาเป็น 10 เพราะเป็นตัวแรก
}



void main() {
  var postfix = stdin.readLineSync()!;
  print(evaluate_posfix(postfix));
}

