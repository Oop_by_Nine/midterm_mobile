import 'dart.io'
bool valid_operator(String synTax) {
  if(synTax == '+'){
    return true;
  }else if(synTax == '-'){
    return true;
  }else if(synTax == '*'){
    return true;
  }else if(synTax == '/'){
    return true;
  }
  return false;
}
bool is_valid_infix_syntax(String synTax){
  if(synTax == '+'){
    return true;
  }else if(synTax == '-'){
    return true;
  }else if(synTax == '*'){
    return true;
  }else if(synTax == '/'){
    return true;
  }else if(double.tryParse(synTax) != null){
    return true;
  }
  return false;
}
int preced(String operator){
  if(operator == '+' || operator == '-'){
    return 1;
  }else if(operator == '*' || operator == '/'){
    return 2;
  }
  return 0;

}
int preced(String operator){
  if(operator == '+' || operator == '-'){
    return 1;
  }else if(operator == '*' || operator == '/'){
    return 2;
  }
  return 0;

}
List infix_to_postfix(String infixExpresstion){
  var express = infixExpresstion;
  var optrs = [];
  var postFix = [];

  for(int i=0; i < express.length; i++){
    var tken = express[i];
    if(!is_valid_infix_syntax(tken))continue;
    if(double.tryParse(tken) != null){
      postFix.add(double.tryParse(tken));
    }else if(valid_operator(tken)){
      while(optrs.isNotEmpty && optrs.last != '(' && preced(tken) < preced(optrs.last)){
        postFix.add(optrs.removeLast());
      }
      optrs.add(tken);
    }else if(tken == '('){
      optrs.add(tken);
    }else if(tken == ')'){
      while(optrs.last != '('){
        postFix.add(optrs.removeLast());
      }
      optrs.removeLast();
    }
  }
    while(optrs.isNotEmpty) {
    postFix.add(optrs.removeLast());
  }
  return postFix;
}
void main() {
  var infixExpressionInput = stdin.readLineSync()!;
  var postFix = infix_to_postfix(infixExpressionInput);
  print("Infix2Postfix: $postFix");
}

  