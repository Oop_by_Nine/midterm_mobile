import 'dart.io'

List convert(String expression, var regExp){
  return expression.split(regExp);
}
//จะนำค่าStringที่ได้มาแปลงให้อยู่ใน list เช่น "2","4","10" = [2,4,10]
void main() {
  print("Token: ");
  var infixExpressionInput = stdin.readLineSync()!;

  print("RegExp for Tokenizing: ");
  var regExp = stdin.readLineSync()!;
  
  var result = convert(infixExpressionInput, regExp);
  print("Result: $result");
}